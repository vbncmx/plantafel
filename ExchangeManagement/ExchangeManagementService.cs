﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Exchange.WebServices.Data;

namespace ExchangeManagement
{
    public class ExchangeManagementService
    {
        private const string NoSemaphore = "Could not own Exchange Semaphore";
        private const int ExchangeSemaphoreCountDefault = 5;
        private static readonly TimeSpan ExchangeSemaphoreTimeoutDefault = TimeSpan.FromHours(1);
        private readonly string _authLogin;
        private readonly string _authPass;
        private readonly string _ewsUri;
        private readonly TimeSpan _exchangeSemaphoreWait;
        private readonly ExchangeVersion _exchangeVersion;

        public static readonly PropertySet PropertySet = new PropertySet(ItemSchema.Importance, ItemSchema.Sensitivity, ItemSchema.HasAttachments,
            ItemSchema.Body, ItemSchema.LastModifiedTime, AppointmentSchema.LegacyFreeBusyStatus, AppointmentSchema.Location,
            ItemSchema.Categories, AppointmentSchema.TimeZone, AppointmentSchema.IsAllDayEvent,
            ItemSchema.Subject, AppointmentSchema.Start, AppointmentSchema.Organizer,
            AppointmentSchema.AppointmentType, AppointmentSchema.End, AppointmentSchema.IsCancelled, AppointmentSchema.ICalUid);

        private readonly SemaphoreSlim _semaphore;

        public ExchangeManagementService(string authLogin, string authPass, string ewsUri) : this(authLogin, authPass, ewsUri, ExchangeSemaphoreCountDefault)
        { }

        public ExchangeManagementService(string authLogin, string authPass, string ewsUri,
            int exchangeSemaphoreCount) : this(authLogin, authPass, ewsUri, ExchangeSemaphoreTimeoutDefault, exchangeSemaphoreCount)
        { }

        public ExchangeManagementService(string authLogin, string authPass, string ewsUri,
            TimeSpan exchangeSemaphoreWait, int exchangeSemaphoreCount) : this(authLogin, authPass, ewsUri, exchangeSemaphoreWait, exchangeSemaphoreCount, ExchangeVersion.Exchange2010_SP1)
        { }

        public ExchangeManagementService(string authLogin, string authPass, string ewsUri,
            ExchangeVersion exchangeVersion) : this(authLogin, authPass, ewsUri, ExchangeSemaphoreTimeoutDefault, ExchangeSemaphoreCountDefault, exchangeVersion)
        { }

        public ExchangeManagementService(string authLogin, string authPass, string ewsUri,
            TimeSpan exchangeSemaphoreWait, int exchangeSemaphoreCount, ExchangeVersion exchangeVersion)
        {
            if (authLogin == null) throw new ArgumentNullException(nameof(authLogin));
            if (authPass == null) throw new ArgumentNullException(nameof(authPass));
            if (ewsUri == null) throw new ArgumentNullException(nameof(ewsUri));
            _authLogin = authLogin;
            _authPass = authPass;
            _ewsUri = ewsUri;
            _exchangeSemaphoreWait = exchangeSemaphoreWait;
            _exchangeVersion = exchangeVersion;
            _semaphore = new SemaphoreSlim(exchangeSemaphoreCount, exchangeSemaphoreCount);
        }

        public async void AddMailboxToContactList(string mailboxId, CancellationToken ct)
        {
            var isSemaphorOwned = false;
            try
            {
                isSemaphorOwned = await _semaphore.WaitAsync(_exchangeSemaphoreWait, ct);
                if (!isSemaphorOwned) throw new Exception(NoSemaphore);

                var service = GetConfiguredService();
                var contact = new Contact(service);
                contact.EmailAddresses[EmailAddressKey.EmailAddress1] = new EmailAddress(mailboxId);
                contact.DisplayName = mailboxId;
                contact.Save();
            }
            finally
            {
                if (isSemaphorOwned)
                    _semaphore.Release();
            }
        }

        public async Task<Appointment[]> GetAllEventsInPeriod(string mailboxId, DateTime start, DateTime end,
            CancellationToken ct)
        {
            var isSemaphorOwned = false;
            try
            {
                isSemaphorOwned = await _semaphore.WaitAsync(_exchangeSemaphoreWait, ct);
                if (!isSemaphorOwned) throw new Exception(NoSemaphore);
                var mailbox = new Mailbox(mailboxId);
                var folderId = new FolderId(WellKnownFolderName.Calendar, mailbox);
                var service = GetConfiguredService();
                return PerformPagedAppointmentSearch(service, folderId, start, end, PropertySet);
            }
            finally
            {
                if (isSemaphorOwned)
                    _semaphore.Release();
            }
        }

        public async Task<string[]> GetAllEventIdsInPeriod(string mailboxId, DateTime start, DateTime end, CancellationToken ct)
        {
            var isSemaphorOwned = false;
            try
            {
                isSemaphorOwned = await _semaphore.WaitAsync(_exchangeSemaphoreWait, ct);
                if (!isSemaphorOwned) throw new Exception(NoSemaphore);
                var propertySet = new PropertySet(BasePropertySet.IdOnly, AppointmentSchema.ICalUid, AppointmentSchema.Start);
                var mailbox = new Mailbox(mailboxId);
                var folderId = new FolderId(WellKnownFolderName.Calendar, mailbox);
                var service = GetConfiguredService();
                var appointments = PerformPagedAppointmentSearch(service, folderId, start, end, propertySet);
                return appointments.Select(i => i.EventId()).ToArray();
            }
            finally
            {
                if (isSemaphorOwned)
                    _semaphore.Release();
            }
        }

        public async Task<string[]> GetContactEmails(CancellationToken ct)
        {
            var isSemaphorOwned = false;
            try
            {
                isSemaphorOwned = await _semaphore.WaitAsync(_exchangeSemaphoreWait, ct);
                if (!isSemaphorOwned)
                    throw new Exception(NoSemaphore);

                var service = GetConfiguredService();
                var view = new ItemView(int.MaxValue)
                {
                    PropertySet =
                        new PropertySet(BasePropertySet.IdOnly, ContactSchema.DisplayName, ContactSchema.EmailAddress1)
                };
                var contactItems = service.FindItems(WellKnownFolderName.Contacts, view).ToArray();

                if (!contactItems.Any())
                    return new string[] { };
                var contacts = contactItems.OfType<Contact>().ToArray();
                if (!contacts.Any())
                    return new string[] { };
                return contacts.Where(c => !string.IsNullOrWhiteSpace(c.DisplayName))
                    .Select(c => c.DisplayName.ToLower()).ToArray();
            }
            finally
            {
                if (isSemaphorOwned)
                    _semaphore.Release();
            }
        }

        public async Task<bool> MailboxExists(string mailbox, CancellationToken ct)
        {
            try
            {
                var dummyStart = new DateTime(1945, 1, 1);
                var dummyEnd = new DateTime(1945, 2, 1);
                return (await GetAllEventIdsInPeriod(mailbox, dummyStart, dummyEnd, ct)).Length >= 0;
            }
            catch (ServiceResponseException exc) when (exc.Message.Contains("no mailbox"))
            {
                return false;
            }
        }

        public async Task<Appointment[]> GetChangedAppointments(string mailboxId, DateTime start, DateTime end, CancellationToken ct, DateTime timeFrameStart, DateTime timeFrameEnd)
        {
            var isSemaphorOwned = false;
            try
            {
                isSemaphorOwned = await _semaphore.WaitAsync(_exchangeSemaphoreWait, ct);
                if (!isSemaphorOwned)
                    throw new Exception(NoSemaphore);

                var searchFilter = new SearchFilter.SearchFilterCollection
                {
                    new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.LastModifiedTime, start),
                    new SearchFilter.IsLessThanOrEqualTo(ItemSchema.LastModifiedTime, end)
                };
                
                var searchPropertySet = new PropertySet(BasePropertySet.IdOnly, AppointmentSchema.ICalUid,
                    AppointmentSchema.AppointmentType, AppointmentSchema.Start);
                var mailbox = new Mailbox(mailboxId);
                var folderId = new FolderId(WellKnownFolderName.Calendar, mailbox);

                var service = GetConfiguredService();
                
                //var items = PerformPagedSearch(service, folderId, searchFilter, PageSize, searchPropertySet);

                var view = new ItemView(int.MaxValue) { PropertySet = searchPropertySet };
                var items = service.FindItems(folderId, searchFilter, view);

                var recurMasters =
                    items.OfType<Appointment>().Where(i => i.AppointmentType == AppointmentType.RecurringMaster).ToArray();

                var appointments = items.OfType<Appointment>()
                    .Where(i => i.AppointmentType != AppointmentType.RecurringMaster
                        && timeFrameStart < i.Start && i.Start < timeFrameEnd).ToArray();

                if (recurMasters.Length > 0)
                {
                    var occurances = EnumerateOccurances(folderId, service, start, end, searchPropertySet, recurMasters);
                    appointments = appointments.Concat(occurances).ToArray();
                }

                if (appointments.Length > 0)
                    service.LoadPropertiesForItems(appointments, PropertySet);

                return appointments;
            }
            finally
            {
                if (isSemaphorOwned)
                    _semaphore.Release();
            }
        }

        private Item[] PerformPagedSearch(ExchangeService service, FolderId folderId, SearchFilter searchFilter,
            int pageSize, PropertySet searchPropertySet)
        {
            Item[] items = { };
            var moreItems = true;
            ItemId anchorId = null;
            var view = new ItemView(pageSize, 0) { PropertySet = searchPropertySet };

            while (moreItems)
            {
                var justFound = service.FindItems(folderId, searchFilter, view);
                moreItems = justFound.MoreAvailable;
                items = items.Concat(justFound).ToArray();

                if (moreItems && anchorId != null)
                {
                    // Check the first result to make sure it matches
                    // the last result (anchor) from the previous page.
                    // If it doesn't, that means that something was added
                    // or deleted since you started the search.
                    // so start from the beginning
                    if (!Equals(justFound.Items.FirstOrDefault()?.Id, anchorId))
                    {
                        view.Offset = 0;
                        anchorId = null;
                        continue;
                    }
                }

                if (moreItems)
                    view.Offset += pageSize;

                anchorId = justFound.Items.LastOrDefault()?.Id;
            }

            return items;
        }

        private Appointment[] PerformPagedAppointmentSearch(ExchangeService service, FolderId folderId, DateTime start,
            DateTime end, PropertySet propertySet)
        {
            Appointment[] appointments = { };
            var periods = BreakIntoPeriods(start, end);
            foreach (var period in periods)
            {
                var view = new CalendarView(period.Item1, period.Item2, int.MaxValue);
                var justFound = service.FindAppointments(folderId, view);
                appointments = appointments.Concat(justFound).ToArray();
            }
            if (appointments.Length > 0)
                service.LoadPropertiesForItems(appointments, propertySet);
            return appointments;
        }

        private Appointment[] EnumerateOccurances(FolderId folderId, ExchangeService service,
            DateTime start, DateTime end, PropertySet propertySet, params Appointment[] recurMasters)
        {
            var appointments = new Appointment[] {};
            var monthPeriods = BreakIntoPeriods(start, end).ToArray();
            foreach (var period in monthPeriods)
            {
                var view = new CalendarView(period.Item1, period.Item2);
                var justFound = service.FindAppointments(folderId, view)
                    .Where(i => recurMasters.Any(m => m.ICalUid == i.ICalUid));
                appointments = appointments.Concat(justFound).ToArray();
            }
            if (appointments.Length > 0)
                service.LoadPropertiesForItems(appointments, propertySet);
            return appointments;
        }

        /// <summary>
        /// http://stackoverflow.com/a/15086409/6483508
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private IEnumerable<Tuple<DateTime, DateTime>> BreakIntoPeriods(DateTime start, DateTime end)
        {
            var interval = TimeSpan.FromDays(90);
            var runningDate = start;
            while (runningDate < end)
            {
                var toCandidate = runningDate + interval;
                var to = end < toCandidate ? end : toCandidate;
                yield return new Tuple<DateTime, DateTime>(runningDate, to);
                runningDate = to;
            }
        }

        private ExchangeService GetConfiguredService()
        {
            var service = new ExchangeService(_exchangeVersion)
            {
                Credentials = new WebCredentials(_authLogin, _authPass),
                Url = new Uri(_ewsUri)
            };
            return service;
        }
    }
}
