﻿using System;
using Microsoft.Exchange.WebServices.Data;

namespace ExchangeManagement
{
    public static class AppointmentExtensions
    {
        public static string EventId(this Appointment appointment)
        {
            return $"{appointment.ICalUid}_{appointment.Start.ToLongDateString().GetHashCode()}" ;
        }
    }
}