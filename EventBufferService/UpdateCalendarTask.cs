using System;

namespace EventBufferService
{
    public class UpdateCalendarTask
    {
        public string CalendarId { get; set; }
        public DateTime ChangePeriodStart { get; set; }
        public DateTime ChangePeriodEnd { get; set; }
        public bool DeletedAppointmentCheckRequired { get; set; }
    }
}