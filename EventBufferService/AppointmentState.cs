namespace EventBufferService
{
    public enum AppointmentState
    {
        Changed, New
    }
}