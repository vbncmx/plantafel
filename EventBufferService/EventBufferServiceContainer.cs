﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using Buffer;
using ExchangeManagement;
using Ipc;
using Microsoft.Exchange.WebServices.Data;
using NLog;

namespace EventBufferService
{
    public class EventBufferServiceContainer
    {
        private readonly string _exchangeServiceUrl = ConfigurationManager.AppSettings["ExchangeServiceUrl"];
        private readonly string _exchangeAuthLogin = ConfigurationManager.AppSettings["ExchangeAuthLogin"];
        private readonly string _exchangeAuthPass = ConfigurationManager.AppSettings["ExchangeAuthPass"];
        private readonly int _timeFrameForwardDays = int.Parse(ConfigurationManager.AppSettings["TimeFrameForwardDays"]);
        private readonly int _timeFrameBackwardDays = int.Parse(ConfigurationManager.AppSettings["TimeFrameBackwardDays"]);

        private readonly EventWaitHandle _contactListUpdateSignal = new EventWaitHandle(false, EventResetMode.AutoReset,
            "66E4B327-CD6D-46D3-B158-5066D4813203");

        private readonly TimeSpan _contactListUpdateInterval = TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["ContactListUpdateIntervalSec"]));
        private readonly TimeSpan _deletedEventsCheckInterval = TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["DeletedEventsCheckIntervalSec"]));
        private readonly TimeSpan _updateEventsInterval = TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["UpdateEventsIntervalSec"]));
        private readonly TimeSpan _exceptionedUpdateEventsInterval = TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["ExceptionedUpdateEventsIntervalSec"]));
        private readonly TimeSpan _exchangeSemaphoreTimeout = TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["ExchangeSemaphoreTimeoutSec"]));
        private readonly TimeSpan _timeFrameCycleInterval = TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["TimeFrameIntervalSec"]));
        private readonly int _exchnageSemaphoreCount =
            int.Parse(ConfigurationManager.AppSettings["ExchangeSemaphoreCount"]);

        private CancellationTokenSource _cancellationTokenSource = null;

        private DeleteEventCycleService _deleteEventCycleService;
        private TimeFrameCycleService _timeFrameCycleService;
        private ContactListCycleService _contactListCycleService;
        private UpdateCycleService _updateCycleService;

        private GridModel _gridModel;
        private readonly object _gridModelLock = new object();

        public void Initialise()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CalendarContext>());
            var exchangeVersion = (ExchangeVersion)Enum.Parse(typeof(ExchangeVersion),
                ConfigurationManager.AppSettings["ExchangeVersion"]);
            var exchangeSvc = new ExchangeManagementService(_exchangeAuthLogin, _exchangeAuthPass, _exchangeServiceUrl,
                _exchangeSemaphoreTimeout, _exchnageSemaphoreCount, exchangeVersion);
            _timeFrameCycleService = new TimeFrameCycleService(_timeFrameCycleInterval, _timeFrameBackwardDays,
                _timeFrameForwardDays);
            _contactListCycleService = new ContactListCycleService(_contactListUpdateInterval, _contactListUpdateSignal,
                exchangeSvc);
            _updateCycleService = new UpdateCycleService(_updateEventsInterval, _exceptionedUpdateEventsInterval,
                exchangeSvc, _timeFrameBackwardDays, _timeFrameForwardDays);

            _updateCycleService.GridRowUpdate += (sender, args) =>
            {
                lock (_gridModelLock)
                {
                    var row = _gridModel?.Rows.FirstOrDefault(r => r.MailboxId == args.MailboxId);
                    if (row != null)
                    {
                        if (args.IsGood)
                            row.Good++;
                        else
                            row.Bad++;
                        row.Error = args.IsBad;
                    }
                }

                IpcMessage.UpdateGrid(_gridModel).Send();
            };
            _updateCycleService.LogMessage += OnLogMessage;
            _updateCycleService.ExceptionReport += OnExceptionReport;

            _deleteEventCycleService = new DeleteEventCycleService(_deletedEventsCheckInterval, exchangeSvc,
                _timeFrameBackwardDays, _timeFrameForwardDays);
            _deleteEventCycleService.ExceptionReport += OnExceptionReport;
            _deleteEventCycleService.LogMessage += OnLogMessage;

            _contactListCycleService.ContactListUpdated += (sender, args) =>
            {
                var mailboxIdsString = string.Join(",", args.MailboxIds);
                lock (_gridModelLock)
                {
                    _gridModel = InitialGridModel(args.MailboxIds);
                }

                IpcMessage.UpdateGrid(_gridModel).Send();

                foreach (var mailboxId in args.MailboxIds)
                {
                    _updateCycleService.StartCycle(mailboxId, args.CancellationToken);
                }
                    
                IpcMessage.LogArea($"Started UPDATE task chains for: {mailboxIdsString}").Send();

                foreach (var mailboxId in args.MailboxIds)
                    _deleteEventCycleService.StartCycle(mailboxId, args.CancellationToken);
                IpcMessage.LogArea($"Started DELETE task chains for: {mailboxIdsString}").Send();

                _timeFrameCycleService.StartCycle(args.CancellationToken);
            };

            _contactListCycleService.ExceptionReport += OnExceptionReport;
        }

        private GridModel InitialGridModel(string[] mailboxIds)
        {
            return new GridModel
            {
                Rows = mailboxIds.Select(i =>
                    new GridRow
                    {
                        Bad = 0,
                        Good = 0,
                        Error = false,
                        MailboxId = i
                    }).ToArray()
            };
        }

        private void OnLogMessage(object sender, LogEventArgs args)
        {
            IpcMessage.LogArea(args.Message).Send();
            LogManager.GetCurrentClassLogger().Info(args.Message);
        }

        private void OnExceptionReport(object sender, ExceptionReportArgs args)
        {
            IpcMessage.LogArea($"EXCEPTION {args.Prefix} {Environment.NewLine}{args.Exception.Message}");
            LogManager.GetCurrentClassLogger().Info($"{args.Prefix} {Environment.NewLine}{args.Exception}");
        }

        public void Start()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _contactListCycleService.StartCycle(_cancellationTokenSource.Token);
        }

        public void Stop()
        {
            _cancellationTokenSource?.Cancel();
        }
    }
}