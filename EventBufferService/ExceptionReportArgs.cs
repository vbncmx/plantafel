using System;

namespace EventBufferService
{
    public class ExceptionReportArgs : EventArgs
    {
        public ExceptionReportArgs(string prefix, Exception exception)
        {
            Prefix = prefix;
            Exception = exception;
        }

        public string Prefix { get; private set; }
        public Exception Exception { get; private set; }
    }
}