namespace EventBufferService
{
    public class LogEventArgs
    {
        public LogEventArgs(string message)
        {
            Message = message;
        }
        public string Message { get; private set; }
    }
}