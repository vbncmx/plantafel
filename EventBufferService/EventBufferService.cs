﻿using System;
using System.CodeDom;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using Buffer;
using ExchangeManagement;
using Ipc;
using Microsoft.Exchange.WebServices.Data;
using NLog;

namespace EventBufferService
{
    public partial class EventBufferService : ServiceBase
    {
        readonly EventBufferServiceContainer _serviceContainer;
        public EventBufferService()
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                EventLog.WriteEntry(args.ExceptionObject.ToString(), EventLogEntryType.Error);
            };

            InitializeComponent();
            _serviceContainer = new EventBufferServiceContainer();
            _serviceContainer.Initialise();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                new CalendarContext().Database.Initialize(false);
                _serviceContainer.Start();
            }
            catch (Exception exc)
            {
                EventLog.WriteEntry(exc.ToString(), EventLogEntryType.Error);
                try { LogManager.GetCurrentClassLogger().Error(exc); } catch { }
            }
            LogManager.GetCurrentClassLogger().Info("NLog test");
            EventLog.WriteEntry("Service Container Started", EventLogEntryType.Information);
        }

        protected override void OnStop()
        {
            try
            {
                _serviceContainer.Stop();
            }
            catch (Exception exc)
            {
                EventLog.WriteEntry(exc.ToString(), EventLogEntryType.Error);
                throw;
            }
            EventLog.WriteEntry("Service Container Stopped", EventLogEntryType.Information);
        }
    }
}
