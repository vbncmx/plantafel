﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Buffer;
using ExchangeManagement;
using Microsoft.Exchange.WebServices.Data;
using NLog;
using Task = System.Threading.Tasks.Task;

namespace EventBufferService
{
    public class DeleteEventCycleService
    {
        public event EventHandler<ExceptionReportArgs> ExceptionReport;
        public event EventHandler<LogEventArgs> LogMessage;

        private readonly TimeSpan _interval;
        private readonly ExchangeManagementService _exchangeService;
        private readonly int _timeFrameBackwardDays;
        private readonly int _timeFrameForwardDays;

        public DateTime TimeFrameStart => DateTime.Now.AddDays(-_timeFrameBackwardDays);
        public DateTime TimeFrameEnd => DateTime.Now.AddDays(_timeFrameForwardDays);

        public DeleteEventCycleService(TimeSpan interval, ExchangeManagementService exchangeService,
            int timeFrameBackwardDays, int timeFrameForwardDays)
        {
            _interval = interval;
            _exchangeService = exchangeService;
            _timeFrameBackwardDays = timeFrameBackwardDays;
            _timeFrameForwardDays = timeFrameForwardDays;
        }

        public void StartCycle(string mailboxId, CancellationToken ct)
        {
            var lastChecked = DateTime.MinValue;
            Action cycleAction = null;
            var debugCounter = 0;

            cycleAction = async () =>
            {
                try
                {
                    var noCheckInterval = DateTime.Now - lastChecked;
                    if (noCheckInterval < _interval)
                        await Task.Delay(_interval - noCheckInterval, ct);

                    debugCounter++;
                    Debug.WriteLine($"StartDeleteCheckTaskChain: {mailboxId} {debugCounter}");

                    var deleted = await PerformDeletedAppointmentCheck(mailboxId, ct);
                    ReportNumberOfDeletedAppointments(deleted, mailboxId);

                    lastChecked = DateTime.Now;
                }
                catch (OperationCanceledException)
                {
                    return;
                }
                catch (ServiceResponseException exc)
                {
                    var args = new ExceptionReportArgs($"DELETE CYCLE {mailboxId} ({exc.ErrorCode})", exc);
                    ExceptionReport?.Invoke(this, args);
                    return;
                }
                catch (Exception exc)
                {
                    var args = new ExceptionReportArgs($"DELETE CYCLE {mailboxId}", exc);
                    ExceptionReport?.Invoke(this, args);
                }

                try
                {
                    if (cycleAction != null)
                        await Task.Factory.StartNew(cycleAction, ct);
                }
                catch (OperationCanceledException) { }

            };
            Task.Factory.StartNew(cycleAction, ct);
        }

        private void ReportNumberOfDeletedAppointments(int deletedNumber, string mailboxId)
        {
            var message = $"MAILBOX: {mailboxId}, MARKED AS DELETED: {deletedNumber}";
            var logger = LogManager.GetCurrentClassLogger();
            logger.Info(message);
            var args = new LogEventArgs($"{DateTime.Now} {message}");
            LogMessage?.Invoke(this, args);
        }

        private async Task<int> PerformDeletedAppointmentCheck(string mailboxId, CancellationToken ct)
        {
            var deletedNumber = 0;
            var context = new CalendarContext();
            var eventRecords = context.CalendarRecords.Where(i => i.CalendarId == mailboxId);
            if (!eventRecords.Any()) return deletedNumber;
            var appointmentIds = await _exchangeService.GetAllEventIdsInPeriod(mailboxId, TimeFrameStart, TimeFrameEnd, ct);
            foreach (var calendarRecord in eventRecords)
            {
                var isDeleted = !appointmentIds.Contains(calendarRecord.EventId);
                calendarRecord.IsDeleted = isDeleted;
                if (isDeleted)
                    deletedNumber++;
            }
            context.SaveChanges();
            return deletedNumber;
        }
    }
}