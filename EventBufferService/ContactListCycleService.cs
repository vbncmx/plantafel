﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ExchangeManagement;
using Microsoft.Exchange.WebServices.Data;
using Nito.AsyncEx;
using Task = System.Threading.Tasks.Task;

namespace EventBufferService
{
    public class ContactListCycleService
    {
        private readonly TimeSpan _interval;
        private readonly EventWaitHandle _updateSignal;
        private readonly ExchangeManagementService _exchangeService;
        public event EventHandler<ContactListEventArgs> ContactListUpdated;
        public event EventHandler<ExceptionReportArgs> ExceptionReport;

        public ContactListCycleService(TimeSpan interval, EventWaitHandle updateSignal, ExchangeManagementService exchangeService)
        {
            if (updateSignal == null) throw new ArgumentNullException(nameof(updateSignal));
            _interval = interval;
            _updateSignal = updateSignal;
            this._exchangeService = exchangeService;
        }

        public void StartCycle(CancellationToken userControlledCt)
        {
            CancellationTokenSource cts = null;
            var lastUpdated = DateTime.MinValue;
            var debugCounter = 0;
            Action cycleAction = null;

            cycleAction = async () =>
            {
                try
                {
                    var noUpdateInterval = DateTime.Now - lastUpdated;
                    if (noUpdateInterval < _interval)
                        await
                            AsyncFactory.FromWaitHandle(_updateSignal,
                                _interval - noUpdateInterval, userControlledCt);

                    debugCounter++;

                    cts?.Cancel();
                    cts = new CancellationTokenSource();

                    var mailboxIds = (await _exchangeService.GetContactEmails(userControlledCt)).Distinct().ToArray();

                    ContactListUpdated?.Invoke(this, new ContactListEventArgs(mailboxIds, cts.Token));

                    lastUpdated = DateTime.Now;
                }
                catch (OperationCanceledException)
                {
                    cts?.Cancel();
                    return;
                }
                catch (ServiceResponseException exc)
                {
                    ExceptionReport?.Invoke(this, new ExceptionReportArgs($"CONTACT CYCLE ({exc.ErrorCode})", exc));
                }
                catch (ServiceRequestException exc)
                {
                    ExceptionReport?.Invoke(this, new ExceptionReportArgs("CONTACT CYCLE", exc));
                }

                try
                {
                    if (cycleAction != null)
                        await Task.Factory.StartNew(cycleAction, userControlledCt);
                }
                catch (OperationCanceledException) { }
            };
            Task.Factory.StartNew(cycleAction, userControlledCt);
        }
    }
}