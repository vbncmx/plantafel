﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Security.AccessControl;
using System.Threading;
using System.Threading.Tasks;
using Buffer;
using ExchangeManagement;
using Ipc;
using Microsoft.Exchange.WebServices.Data;
using NLog;
using Task = System.Threading.Tasks.Task;

namespace EventBufferService
{
    public class UpdateCycleService
    {
        public event EventHandler<ExceptionReportArgs> ExceptionReport;
        public event EventHandler<LogEventArgs> LogMessage;
        public event EventHandler<GridRowEventArgs> GridRowUpdate;

        private readonly TimeSpan _normalInterval;
        private readonly TimeSpan _exceptionedInterval;
        private readonly ExchangeManagementService _exchangeService;
        private readonly int _timeFrameBackwardDays;
        private readonly int _timeFrameForwardDays;

        public DateTime TimeFrameStart => DateTime.Now.AddDays(-_timeFrameBackwardDays);
        public DateTime TimeFrameEnd => DateTime.Now.AddDays(_timeFrameForwardDays);

        public UpdateCycleService(TimeSpan normalInterval, TimeSpan exceptionedInterval, ExchangeManagementService exchangeService, int timeFrameBackwardDays, int timeFrameForwardDays)
        {
            if (exchangeService == null) throw new ArgumentNullException(nameof(exchangeService));
            _normalInterval = normalInterval;
            _exceptionedInterval = exceptionedInterval;
            _exchangeService = exchangeService;
            _timeFrameBackwardDays = timeFrameBackwardDays;
            _timeFrameForwardDays = timeFrameForwardDays;
        }

        public void StartCycle(string mailboxId, CancellationToken ct)
        {
            var context = new CalendarContext();
            var start = GetUpdatePeriodStart(mailboxId, context);
            var end = DateTime.Now;
            var debugCounter = 0;
            Action cycleAction = null;
            var updateInterval = _normalInterval;
            var bufferLock = new object();

            cycleAction = async () =>
            {
                try
                {
                    var noUpdateInterval = DateTime.Now - end;
                    if (noUpdateInterval < updateInterval)
                        await Task.Delay(updateInterval - noUpdateInterval, ct);

                    debugCounter++;
                    Debug.WriteLine($"StartUpdateCycleTaskChain: {mailboxId} {debugCounter}");

                    var intermediates = await ReadExchange(mailboxId, start, end, ct);

                    Tuple<int, int> updateBufferResult;
                    List<Exception> exceptions;
                    lock (bufferLock)
                    {
                        updateBufferResult = UpdateBuffer(intermediates, out exceptions);
                    }
                    if (exceptions.Any())
                        throw new AggregateException(exceptions);

                    SignalContactUpdate(mailboxId);

                    ReportTaskResult(mailboxId, start, end, updateBufferResult.Item1, updateBufferResult.Item2);

                    GridRowUpdate?.Invoke(this, GridRowEventArgs.Good(mailboxId));

                    start = end.Offset();
                    end = DateTime.Now;
                    updateInterval = _normalInterval; // set normal update rate
                }
                catch (OperationCanceledException)
                {
                    return;
                }
                catch (ServiceObjectPropertyException exc)
                {
                    ExceptionReport?.Invoke(this, new ExceptionReportArgs($"UPDATE CYCLE {mailboxId} (Property Error: {exc.PropertyDefinition})", exc));
                    GridRowUpdate?.Invoke(this,GridRowEventArgs.Bad(mailboxId));
                    end = DateTime.Now;
                    updateInterval = _exceptionedInterval; // slow down update rate
                }
                catch (ServiceResponseException exc)
                {
                    ExceptionReport?.Invoke(this, new ExceptionReportArgs($"UPDATE CYCLE {mailboxId} ({exc.ErrorCode})", exc));
                    GridRowUpdate?.Invoke(this, GridRowEventArgs.Bad(mailboxId));
                    end = DateTime.Now;
                    updateInterval = _exceptionedInterval; // slow down update rate
                }
                catch (Exception exc)
                {
                    ExceptionReport?.Invoke(this, new ExceptionReportArgs($"UPDATE CYCLE {mailboxId}", exc));
                    GridRowUpdate?.Invoke(this, GridRowEventArgs.Bad(mailboxId));
                    end = DateTime.Now;
                    updateInterval = _exceptionedInterval; // slow down update rate
                }

                try
                {
                    if (cycleAction != null)
                        await Task.Factory.StartNew(cycleAction, ct);
                }
                catch (OperationCanceledException) { }
            };
            Task.Factory.StartNew(cycleAction, ct);
        }

        private async Task<UpdateCalendarIntermediate[]> ReadExchange(string mailboxId, DateTime? start, DateTime end, CancellationToken ct)
        {
            Appointment[] appointments = { };
            if (start == null)
            {
                appointments = await _exchangeService.GetAllEventsInPeriod(mailboxId, TimeFrameStart, TimeFrameEnd, ct);
            }
            else
            {
                appointments = await _exchangeService.GetChangedAppointments(mailboxId, start.Value, end, ct, TimeFrameStart, TimeFrameEnd);
            }
            return appointments.Select(appointment => new UpdateCalendarIntermediate
            {
                Appointment = appointment,
                CalendarId = mailboxId,
                ChangePeriodEnd = end
            })
            .ToArray();
        }

        private DateTime? GetUpdatePeriodStart(string mailboxId, CalendarContext context)
        {
            var records = context.CalendarRecords.Where(r => r.CalendarId == mailboxId);
            return records.Any()
                ? records.Max(r => r.ChangePeriodEnd).Offset()
                : (DateTime?)null;
        }

        private void SignalContactUpdate(string mailboxId)
        {
            var signalEventHandleName = $"ContactDataUpdated_{mailboxId}";
            EventWaitHandle signalWaitHandle;
            EventWaitHandle.TryOpenExisting(signalEventHandleName, EventWaitHandleRights.Modify,
                out signalWaitHandle);
            signalWaitHandle?.Set();
        }

        private Tuple<int, int> UpdateBuffer(UpdateCalendarIntermediate[] intermediates, out List<Exception> exceptions)
        {
            var updatedRecords = 0;
            var newRecords = new List<CalendarRecord>();
            var context = new CalendarContext();
            exceptions = new List<Exception>();
            foreach (var intermediate in intermediates)
            {
                var eventId = intermediate.Appointment.EventId();
                var isAlreadyAdded = newRecords.Any(r => r.EventId == eventId && r.CalendarId == intermediate.CalendarId);
                if (isAlreadyAdded)
                    continue;

                var matchingRecord = context.CalendarRecords
                    .FirstOrDefault(r => r.EventId == eventId && r.CalendarId == intermediate.CalendarId);

                if (matchingRecord == null)
                {
                    var newRecord = new CalendarRecord();
                    try
                    {
                        intermediate.Apply(newRecord);
                        newRecords.Add(newRecord);
                    }
                    catch (Exception exc)
                    {
                        var servObjPropExc = exc as ServiceObjectPropertyException;
                        if (servObjPropExc != null)
                        {
                            exc = new Exception($"Property Exception: {servObjPropExc.PropertyDefinition}", servObjPropExc);
                        }
                        exceptions.Add(exc);
                        continue;
                    }
                }
                else
                {
                    try
                    {
                        intermediate.Apply(matchingRecord);
                        updatedRecords++;
                    }
                    catch (Exception exc)
                    {
                        exceptions.Add(exc);
                        continue;
                    }
                }
            }

            if (newRecords.Any())
                context.CalendarRecords.AddRange(newRecords);
            context.SaveChanges();

            return new Tuple<int, int>(newRecords.Count, updatedRecords);
        }

        private void ReportTaskResult(string mailboxId, DateTime? start, DateTime end, int insertedRecords, int updatedRecords)
        {
            var startString = start?.ToString("dd-MM-yyyy HH:mm:ss");
            var endString = end.ToString("dd-MM-yyyy HH:mm:ss");
            var message = $"MAILBOX: {mailboxId}, Update Period: {startString} - {endString}, RESULT: UPDATED={updatedRecords}, INSERTED={insertedRecords}.\n ";
            //var logger = LogManager.GetCurrentClassLogger();
            //logger.Info(message);

            LogMessage?.Invoke(this, new LogEventArgs(message));
        }
    }

    public static class UsefulExtensions
    {
        private static readonly TimeSpan UpdatePeriodOffset = TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["UpdatePeriodOffsetSec"]));
        public static DateTime Offset(this DateTime dateTime)
        {
            return dateTime - UpdatePeriodOffset;
        }
    }
}