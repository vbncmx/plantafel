﻿using System;
using System.Linq.Expressions;
using Buffer;
using ExchangeManagement;
using Microsoft.Exchange.WebServices.Data;
using NLog;

namespace EventBufferService
{
    public static class IndermediateExtensions
    {
        public static CalendarRecord Apply(this UpdateCalendarIntermediate intermediate, CalendarRecord calendarRecord)
        {
            var calendarId = intermediate.CalendarId;
            var appointment = intermediate.Appointment;
            var sensitivity = GetValue(appointment, i => i.Sensitivity, Sensitivity.Private);
            var isPrivate = sensitivity == Sensitivity.Private;
            var busyStatus = GetValue(appointment, i => i.LegacyFreeBusyStatus, LegacyFreeBusyStatus.NoData).ToString();
            if (isPrivate)
            {
                calendarRecord.CalendarId = calendarId;
                calendarRecord.EventId = appointment.EventId();
                calendarRecord.AllDayEvent = false;
                calendarRecord.Body = null;
                calendarRecord.BusyStatus = busyStatus;
                calendarRecord.Categories = null;
                calendarRecord.End = appointment.End;
                calendarRecord.Start = appointment.Start;
                calendarRecord.HasAttachements = false;
                calendarRecord.IsCanceled = false;
                calendarRecord.IsPrivate = true;
                calendarRecord.Subject = null;
                calendarRecord.OrganizerEmail = null;
                calendarRecord.OrganizerName = null;
                calendarRecord.Priority = null;
                calendarRecord.Location = null;
                calendarRecord.TimeZone = null;
                calendarRecord.ChangePeriodEnd = intermediate.ChangePeriodEnd;

                // tracking times both on exchange and local
                calendarRecord.LastChangeTimeExchange = appointment.LastModifiedTime;
                calendarRecord.LastChangeTime = DateTime.Now;
            }
            else
            {
                calendarRecord.CalendarId = calendarId;
                calendarRecord.EventId = appointment.EventId();
                calendarRecord.AllDayEvent = GetValueWithExceptionLogging(appointment, i => i.IsAllDayEvent);
                calendarRecord.Body = null; //appointment.Body.Text;
                calendarRecord.BusyStatus = busyStatus;
                calendarRecord.Categories = appointment.Categories == null
                    ? null
                    : string.Join(",", appointment.Categories);
                calendarRecord.End = GetValueWithExceptionLogging(appointment, i => i.End);
                calendarRecord.Start = GetValueWithExceptionLogging(appointment, i => i.Start);
                calendarRecord.HasAttachements = GetValueWithExceptionLogging(appointment, i => i.HasAttachments);
                calendarRecord.IsCanceled = GetValueWithExceptionLogging(appointment, i => i.IsCancelled);
                calendarRecord.IsPrivate = false;
                calendarRecord.Subject = GetValueWithExceptionLogging(appointment, i => i.Subject);
                calendarRecord.OrganizerEmail = appointment.Organizer?.Address;
                calendarRecord.OrganizerName = appointment.Organizer?.Name;
                calendarRecord.Priority = appointment.Importance.ToString();
                calendarRecord.Location = GetValue(appointment, i => i.Location, "");
                calendarRecord.TimeZone = GetValueWithExceptionLogging(appointment, i => i.TimeZone);
                calendarRecord.ChangePeriodEnd = intermediate.ChangePeriodEnd;

                // tracking times both on exchange and local
                calendarRecord.LastChangeTimeExchange = appointment.LastModifiedTime;
                calendarRecord.LastChangeTime = DateTime.Now;
            }
            return calendarRecord;
        }

        private static T GetValue<T>(Appointment appointment, Expression<Func<Appointment, T>> expression, T defaultValue)
        {
            var propertyName = ((MemberExpression)expression.Body).Member.Name;
            var property = typeof (Appointment).GetProperty(propertyName);
            try
            {
                return (T) property.GetValue(appointment);
            }
            catch (Exception)
            {
                return defaultValue;
            }

        }

        private static T GetValueWithExceptionLogging<T>(Appointment appointment, Expression<Func<Appointment, T>> expression)
        {
            var propertyName = ((MemberExpression)expression.Body).Member.Name;
            var property = typeof(Appointment).GetProperty(propertyName);
            try
            {
                return (T)property.GetValue(appointment);
            }
            catch (Exception exc)
            {
                LogManager.GetCurrentClassLogger().Error($"Exchange Property Error ({propertyName}), {exc}");
                throw;
            }

        }
    }
}