﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Buffer;

namespace EventBufferService
{
    public class TimeFrameCycleService
    {
        public event EventHandler<ExceptionReportArgs> ExceptionReport;

        private readonly TimeSpan _interval;
        private readonly int _timeFrameBackwardDays;
        private readonly int _timeFrameForwardDays;

        public DateTime TimeFrameStart => DateTime.Now.AddDays(-_timeFrameBackwardDays);
        public DateTime TimeFrameEnd => DateTime.Now.AddDays(_timeFrameForwardDays);

        public TimeFrameCycleService(TimeSpan interval, int timeFrameBackwardDays, int timeFrameForwardDays)
        {
            _interval = interval;
            _timeFrameBackwardDays = timeFrameBackwardDays;
            _timeFrameForwardDays = timeFrameForwardDays;
        }

        public void StartCycle(CancellationToken ct)
        {
            var lastChecked = DateTime.MinValue;
            Action cycleAction = null;

            cycleAction = async () =>
            {
                try
                {
                    var noCheckInterval = DateTime.Now - lastChecked;
                    if (noCheckInterval < _interval)
                        await Task.Delay(_interval - noCheckInterval, ct);

                    var context = new CalendarContext();
                    var recordsToDelete =
                        context.CalendarRecords.Where(i => i.Start < TimeFrameStart || i.Start > TimeFrameEnd);
                    context.CalendarRecords.RemoveRange(recordsToDelete);
                    context.SaveChanges();
                }
                catch (Exception exc)
                {
                    ExceptionReport?.Invoke(this, new ExceptionReportArgs("TIME FRAME CYCLE ", exc));
                }

                try
                {
                    if (cycleAction != null)
                        await Task.Factory.StartNew(cycleAction, ct);
                }
                catch (OperationCanceledException) { }

            };
            Task.Factory.StartNew(cycleAction, ct);
        }
    }
}