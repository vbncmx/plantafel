using System;
using System.Threading;

namespace EventBufferService
{
    public class ContactListEventArgs : EventArgs
    {
        public ContactListEventArgs(string[] mailboxIds, CancellationToken cancellationToken)
        {
            MailboxIds = mailboxIds;
            CancellationToken = cancellationToken;
        }

        public CancellationToken CancellationToken { get; private set; }
        public string[] MailboxIds { get; private set; }
    }
}