using System;
using Microsoft.Exchange.WebServices.Data;

namespace EventBufferService
{
    public class UpdateCalendarIntermediate
    {
        public Appointment Appointment { get; set; }
        public string CalendarId { get; set; }
        public DateTime ChangePeriodEnd { get; set; }
    }
}