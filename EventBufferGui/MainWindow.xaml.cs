﻿using System.Configuration;
using System.Linq;
using System.Threading;
using System.Windows;
using Ipc;

namespace EventBufferGui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly int _logAreaSize = int.Parse(ConfigurationManager.AppSettings["LogAreaSize"]);
        private bool _stopRequired;

        public MainWindow()
        {
            InitializeComponent();
            StartStopButton.Click += StartStopButtonOnClick;
            Title = "Event Buffer Gui";
            Closing += (sender, args) =>
            {
                _stopRequired = true;
            };
        }

        private void StartStopButtonOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (!_stopRequired) // means the manager is active
            {
                _stopRequired = true;
                StartStopButton.Content = "Start";
            }
            else // so the manager is inactive
            {
                _stopRequired = false;
                StartStopButton.Content = "Stop";
                ThreadPool.QueueUserWorkItem(s =>
                {
                    while (true)
                    {
                        var ipcMessage = IpcExtensions.Receive();
                        if (ipcMessage == null)
                        {
                            if (_stopRequired)
                                break;

                            continue;
                        }

                        if (_stopRequired)
                            break;

                        ProcessIpcMessage(ipcMessage);
                    }
                });
            }
        }

        private void ProcessIpcMessage(IpcMessage ipcMessage)
        {
            if (ipcMessage == null) return;

            if (ipcMessage.LogAreaMessage)
            {
                DispatchToLogArea(ipcMessage.Data);
            }
            else if (ipcMessage.GridUpdate)
            {
                MailboxGrid.Dispatcher.Invoke(() =>
                {
                    MailboxGrid.ItemsSource = ipcMessage.GridModel.Rows
                        .OrderByDescending(r => r.Error ? 0 : 1)
                        .ThenByDescending(r => r.Good);
                });
            }
        }

        private void DispatchToLogArea(string text)
        {
            Dispatcher.Invoke(() =>
            {
                text = $"{text}\n{InfoBlock.Text}";
                InfoBlock.Text = text.Substring(0, text.Length < _logAreaSize ? text.Length : _logAreaSize);
            });
        }
    }
}
