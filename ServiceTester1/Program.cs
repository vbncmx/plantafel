﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ServiceTester1.CalendarService;
using ServiceTester1.DataLayer;

namespace ServiceTester1
{
    class Program
    {
        static void Main(string[] args)
        {
            var aS = ConfigurationManager.AppSettings;
            var start = DateTime.ParseExact(aS["StartDt"], "dd.MM.yyyy HH:mm:ss", CultureInfo.CurrentCulture);
            var end = DateTime.ParseExact(aS["EndDt"], "dd.MM.yyyy HH:mm:ss", CultureInfo.CurrentCulture);
            var mailboxIds = aS["MailboxIds"].Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
            var sleepSec = int.Parse(ConfigurationManager.AppSettings["SleepSec"]);
            while (true)
            {
                var service = new GetCalendarClient();
                var now = DateTime.Now;
                foreach (var mailboxId in mailboxIds)
                {
                    Console.WriteLine($"Checking appointments for {mailboxId}, {start} - {end} ... ");
                    var calendarEvents = service.GET_CALENDAR(mailboxId, start, end).ToArray();
                    var context = new ServiceTester1Context();
                    var moment = new Moment
                    {
                        AppointmentsTotal = calendarEvents.Length,
                        MomentDate = now,
                        MailboxId = mailboxId,
                        EventRecords = new List<EventRecord>()
                    };
                    foreach (var calendarEvent in calendarEvents)
                    {
                        moment.EventRecords.Add(new EventRecord
                        {
                            EventId = calendarEvent.EventID,
                            Start = calendarEvent.start
                        });
                    }
                    context.Moments.Add(moment);
                    context.SaveChanges();
                    Console.Write("Done");
                }
                Thread.Sleep(TimeSpan.FromSeconds(sleepSec));
            }
        }
    }
}
