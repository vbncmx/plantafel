﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace ServiceTester1.DataLayer
{
    public class Moment
    {
        public int MomentId { get; set; }
        public DateTime MomentDate { get; set; }
        public int AppointmentsTotal { get; set; }
        public string MailboxId { get; set; }

        [InverseProperty(nameof(EventRecord.Moment))]
        public virtual List<EventRecord> EventRecords { get; set; }
    }

    public class EventRecord
    {
        [Key, Column(Order = 1)]
        public string EventId { get; set; }

        [Key, Column(Order = 2)]
        public int MomentId { get; set; }
        [ForeignKey(nameof(MomentId))]
        public virtual Moment Moment { get; set; }

        public DateTime Start { get; set; }
    }

    public class ServiceTester1Context : DbContext
    {
        public DbSet<Moment> Moments { get; set; }
        public DbSet<EventRecord> EventRecords { get; set; }   
    }
}