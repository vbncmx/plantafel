﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Channels;
using System.Windows;

using TestTool.GetCalendarSr;
using static System.String;

namespace TestTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            SendBtn.Click += SendBtnOnClick;
        }

        private void SendBtnOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (StartDp.Value == null || EndDp.Value == null)
            {
                MessageBox.Show("Start & End are required.");
                return;
            }
            var start = StartDp.Value.Value;
            var end = EndDp.Value.Value;
            var email = EmailTb.Text;
#if DEBUG
            var service = new GetCalendarClient("IISExpress_Binding");
#else
            var service = new GetCalendarClient("Production_Binding");
#endif
            var sw = new Stopwatch();
            sw.Start();
            var appointments = new ObservableCollection<AppointmentGridRow>(
                service.GET_CALENDAR(email, start, end).Select(AppointmentGridRow.Construct).ToList());
            sw.Stop();
            ResponseInfo.Text = $"Elapsed: {sw.Elapsed}";
            AppointmentGrid.ItemsSource = appointments;
        }
    }

    /// <summary>
    /// A class for displaying in a data grid
    /// </summary>
    class AppointmentGridRow
    {
        //public string EventId { get; set; }
        public string OrganizerName { get; set; }
        public string OrganizerEmail { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string BusyStatus { get; set; }
        public bool AllDayEvent { get; set; }
        public string TimeZone { get; set; }
        public string Subject { get; set; }
        public string Location { get; set; }
        //public string Body { get; set; }
        public string Priority { get; set; }
        public string Categories { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsCanceled { get; set; }
        public bool HasAttachements { get; set; }

        public static AppointmentGridRow Construct(CalendarEvent calendarEvent)
        {
            return new AppointmentGridRow
            {
                //EventId = calendarEvent.EventID,
                OrganizerName = calendarEvent.Organizer?.name,
                OrganizerEmail = calendarEvent.Organizer?.email,
                Start = calendarEvent.start,
                End = calendarEvent.end,
                BusyStatus = calendarEvent.busyStatus,
                AllDayEvent = calendarEvent.allDayEvent,
                TimeZone = calendarEvent.timeZone,
                Subject = calendarEvent.subject,
                Location = calendarEvent.location,
                //Body = calendarEvent.body,
                Priority = calendarEvent.priority,
                Categories = Join(",", calendarEvent.Categories),
                IsPrivate = calendarEvent.isPrivate,
                IsCanceled = calendarEvent.isCanceled,
                HasAttachements = calendarEvent.hasAttachments
            };
        }
    }
}
