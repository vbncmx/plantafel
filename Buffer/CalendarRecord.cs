﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;

namespace Buffer
{
    public class CalendarRecord
    {
        [Key]
        public int RecordId { get; set; }

        [MaxLength(450), Index("IX_CalendarIdEventId", Order = 1, IsUnique = true)]
        public string EventId { get; set; }

        [MaxLength(100), Index("IX_CalendarIdEventId", Order = 2, IsUnique = true)]
        public string CalendarId { get; set; }

        public string OrganizerName { get; set; }
        public string OrganizerEmail { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string BusyStatus { get; set; }
        public bool AllDayEvent { get; set; }
        public string TimeZone { get; set; }
        public string Subject { get; set; }
        public string Location { get; set; }
        public string Body { get; set; }
        public string Priority { get; set; }
        public string Categories { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsCanceled { get; set; }
        public bool HasAttachements { get; set; }
        public DateTime ChangePeriodEnd { get; set; }
        public bool IsDeleted { get; set; }

        public DateTime LastChangeTimeExchange { get; set; }
        public DateTime LastChangeTime { get; set; }

    }
}
