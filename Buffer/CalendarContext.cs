﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.Validation;
using System.Linq;

namespace Buffer
{
    public class CalendarContext : DbContext
    {
        public DbSet<CalendarRecord> CalendarRecords { get; set; }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);
        //    modelBuilder.Entity<CalendarRecord>().Property(e => e.CalendarId)
        //        .HasColumnAnnotation(IndexAnnotation.AnnotationName,
        //        new IndexAnnotation(new IndexAttribute("IX_CalendarIdEventId", 1) { IsUnique = true }));
        //    modelBuilder.Entity<CalendarRecord>().Property(e => e.EventId)
        //        .HasColumnAnnotation(IndexAnnotation.AnnotationName,
        //        new IndexAnnotation(new IndexAttribute("IX_CalendarIdEventId", 2) { IsUnique = true }));
        //}
    }
}