namespace Ipc
{
    public class GridRowEventArgs
    {
        public GridRowEventArgs(){ }

        private GridRowEventArgs(string mailboxId, bool good)
        {
            MailboxId = mailboxId;
            IsGood = good;
        }

        public string MailboxId { get; set; }
        public bool IsGood { get; set; }
        public bool IsBad => !IsGood;

        public static GridRowEventArgs Good(string mailboxId)
        {
            return new GridRowEventArgs(mailboxId, true);
        }

        public static GridRowEventArgs Bad(string mailboxId)
        {
            return new GridRowEventArgs(mailboxId, false);
        }
    }
}