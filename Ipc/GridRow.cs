namespace Ipc
{
    public class GridRow
    {
        public string MailboxId { get; set; }
        public bool Error { get; set; }
        public int Good { get; set; }
        public int Bad { get; set; }
    }
}