using System;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using NLog;

namespace Ipc
{
    public static class IpcExtensions
    {
        private const string MmfId = "Global\\1739F900-F1A3-4045-911F-E6FC1F9D7A0A";
        private static readonly MemoryMappedFile OutcomingMmf = CreateOutcomingMmf();
        private const string IpcSignalId = "Global\\6C977E31-23B1-4A45-987D-1EBE02318628";
        private static readonly EventWaitHandle OutcomingSignal = CreateOutcomingSignal();
        private static readonly object SendLock = new object();
        private const int MmfLengthBytes = 100000;

        public static void Send(this IpcMessage ipcMessage)
        {
            lock (SendLock)
            {
                var ipcMessageJson = ipcMessage.ToJson();

                if (ipcMessage.GridUpdate)
                    LogManager.GetCurrentClassLogger().Debug($"ipcMessageJsonLength: {ipcMessageJson.Length}");

                var buffer = Encoding.UTF8.GetBytes(ipcMessageJson);
                var accessor = OutcomingMmf.CreateViewAccessor();
                accessor.Write(0, (ushort)buffer.Length);
                accessor.WriteArray(2, buffer, 0, buffer.Length);
                OutcomingSignal.Set();
            }
        }

        public static IpcMessage Receive()
        {
            EventWaitHandle incomingSignal = null;
            var ipcSignalResult = EventWaitHandle.TryOpenExisting(IpcSignalId, EventWaitHandleRights.Synchronize | EventWaitHandleRights.Modify,
                out incomingSignal);
            if (!ipcSignalResult)
                return null;
            var waitOne = incomingSignal.WaitOne(1000);
            if (!waitOne)
            {
                return null;
            }
            try
            {
                var incomingMmf = MemoryMappedFile.OpenExisting(MmfId, MemoryMappedFileRights.Read);

                var accessor = incomingMmf.CreateViewAccessor(0, MmfLengthBytes, MemoryMappedFileAccess.Read);
                var size = accessor.ReadUInt16(0);
                var buffer = new byte[size];
                accessor.ReadArray(2, buffer, 0, size);
                var pipeMessageJson = Encoding.UTF8.GetString(buffer);
                var serializer = new JavaScriptSerializer();
                return serializer.Deserialize<IpcMessage>(pipeMessageJson);
            }
            catch(Exception exc)
            {
                Debug.WriteLine(exc.Message);
                return null;
            }
        }

        private static MemoryMappedFile CreateOutcomingMmf()
        {
            try
            {
                var security = new MemoryMappedFileSecurity();
                var sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                var rule = new AccessRule<MemoryMappedFileRights>(sid, MemoryMappedFileRights.FullControl,
                    AccessControlType.Allow);
                security.AddAccessRule(rule);
                return MemoryMappedFile.CreateOrOpen(MmfId, MmfLengthBytes, MemoryMappedFileAccess.ReadWrite, MemoryMappedFileOptions.None,
                    security, HandleInheritability.Inheritable);
            }
            catch (Exception exc)
            {
                LogManager.GetCurrentClassLogger().Error(exc.ToString);
                throw;
            }
        }

        private static EventWaitHandle CreateOutcomingSignal()
        {
            bool _new;
            var security = GetSecurity();
            return new EventWaitHandle(false,
                EventResetMode.AutoReset, IpcSignalId, out _new, security);
        }

        private static EventWaitHandleSecurity GetSecurity()
        {
            var sid = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            var rule = new EventWaitHandleAccessRule(sid,
                EventWaitHandleRights.FullControl,
                AccessControlType.Allow);
            var security = new EventWaitHandleSecurity();
            security.AddAccessRule(rule);
            return security;
        }
    }
}