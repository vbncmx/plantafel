using System;
using System.Web.Script.Serialization;

namespace Ipc
{
    public class IpcMessage
    {
        public bool LogAreaMessage { get; set; }
        public bool GridUpdate { get; set; }
        public bool StopRequired { get; set; }
        public string Data { get; set; }
        public GridModel GridModel { get; set; }

        public string ToJson()
        {
            var serializer = new JavaScriptSerializer();
            return serializer.Serialize(this);
        }

        public static IpcMessage LogArea(string data)
        {
            return new IpcMessage
            {
                LogAreaMessage = true,
                Data = data
            };
        }

        public static IpcMessage UpdateGrid(GridModel gridModel)
        {
            return new IpcMessage
            {
                GridUpdate = true,
                LogAreaMessage = false,
                StopRequired = false,
                GridModel = gridModel
            };
        }

        public static IpcMessage Stop()
        {
            return new IpcMessage
            {
                StopRequired = true,
                Data = "STOP"
            };
        }

        public string[] GetMailboxIds()
        {
            return Data.Split(new [] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public GridRowEventArgs GetGridRowArgs()
        {
            var serializer = new JavaScriptSerializer();
            return serializer.Deserialize<GridRowEventArgs>(Data);
        }
    }
}