﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using ExchangeManagement;
using Microsoft.Exchange.WebServices.Data;

namespace ExchangeTest
{
    class Program
    {
        private static readonly string ExchangeServiceUrl = ConfigurationManager.AppSettings["ExchangeServiceUrl"];
        private static readonly string ExchangeAuthLogin = ConfigurationManager.AppSettings["ExchangeAuthLogin"];
        private static readonly string ExchangeAuthPass = ConfigurationManager.AppSettings["ExchangeAuthPass"];

        static void Main(string[] args)
        {
            var service = new ExchangeManagementService(ExchangeAuthLogin, ExchangeAuthPass, ExchangeServiceUrl);
            while (true)
            {
                try
                {
                    Console.WriteLine("Get appointments, changed in period");
                    Console.WriteLine("mailboxId:");
                    var mailboxId = Console.ReadLine();
                    var start = GetDateFromConsole("start", "dd-MM-yyyy");
                    var end = GetDateFromConsole("end", "dd-MM-yyyy").AddSeconds(24*3600 - 1);
                    var appointments = service.GetChangedAppointments(mailboxId, start, end, CancellationToken.None,
                        new DateTime(1900, 1, 1), new DateTime(2050, 1, 1)).Result;
                    Console.WriteLine($"Loaded {appointments.Length} from Exchange");


                    var errorProperties = new List<PropertyDefinitionBase>();
                    for (var i = 0; i < appointments.Length; i++)
                    {
                        Console.WriteLine($"Appointment {i}:");
                        foreach (var propertyDefinition in ExchangeManagementService.PropertySet)
                        {
                            bool propertyError;
                            var valueStr = GetValue(appointments[i], propertyDefinition, out propertyError);
                            Console.WriteLine($"{propertyDefinition}: {valueStr}");
                            if (propertyError && !errorProperties.Contains(propertyDefinition))
                            {
                                errorProperties.Add(propertyDefinition);
                            }

                        }

                        Console.WriteLine("----------------------------------");
                    }

                    Console.WriteLine(
                        $"Error properties: {string.Join(", ", errorProperties.Select(i => i.ToString()))}");
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.Message);
                }

                Console.WriteLine("One more time (y/n) ?");
                if (Console.ReadLine() != "y")
                    break;
            }
        }

        private static string GetValue(Appointment appointment, PropertyDefinitionBase propertyDefinition, out bool propertyError)
        {
            try
            {
                propertyError = false;
                return typeof(Appointment).GetProperty(propertyDefinition.ToString()).GetValue(appointment).ToString();
            }
            catch (Exception exc)
            {
                propertyError = true;
                return exc.Message;
            }
        }

        private static DateTime GetDateFromConsole(string invitation, string format)
        {
            while (true)
            {
                Console.WriteLine($"{invitation} ({format}):");
                var dateStr = Console.ReadLine();
                try
                {
                    return DateTime.ParseExact(dateStr, format, CultureInfo.InvariantCulture);
                }
                catch(Exception exc)
                {
                    Console.WriteLine(exc.Message);
                }
            }
        }
    }
}
