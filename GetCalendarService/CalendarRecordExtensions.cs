﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Buffer;

namespace GetCalendarService
{
    public static class CalendarRecordExtensions
    {
        public static CalendarEvent ToCalendarEvent(this CalendarRecord calendarRecord)
        {
            var categories = string.IsNullOrEmpty(calendarRecord.Categories)
                ? new List<string>()
                : calendarRecord.Categories.Split(',').ToList();
            return new CalendarEvent
            {
                EventId = calendarRecord.EventId,
                AllDayEvent = calendarRecord.AllDayEvent,
                Body = calendarRecord.Body,
                BusyStatus = calendarRecord.BusyStatus,
                Categories = categories,
                End = calendarRecord.End,
                Start = calendarRecord.Start,
                Subject = calendarRecord.Subject,
                HasAttachements = calendarRecord.HasAttachements,
                IsCanceled = calendarRecord.IsCanceled,
                Organizer = new Organizer
                {
                    Email = calendarRecord.OrganizerEmail,
                    Name = calendarRecord.OrganizerName
                },
                TimeZone = calendarRecord.TimeZone,
                Location = calendarRecord.Location,
                Priority = calendarRecord.Priority,
                IsPrivate = calendarRecord.IsPrivate,
            };
        }
    }
}