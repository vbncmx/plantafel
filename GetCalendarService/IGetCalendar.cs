﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace GetCalendarService
{
    [ServiceContract]
    public interface IGetCalendar
    {
        [OperationContract]
        List<CalendarEvent> GET_CALENDAR(string email, DateTime start, DateTime end);
    }
}