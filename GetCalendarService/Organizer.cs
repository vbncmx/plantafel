﻿using System.Runtime.Serialization;
using Microsoft.Exchange.WebServices.Data;

namespace GetCalendarService
{
    [DataContract]
    public class Organizer
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        public static Organizer FromEmailAddress(EmailAddress emailAddress)
        {
            return new Organizer
            {
                Email = emailAddress.Address,
                Name = emailAddress.Name
            };
        }
    }
}