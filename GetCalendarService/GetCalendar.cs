﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using Buffer;
using ExchangeManagement;
using Microsoft.Exchange.WebServices.Data;
using NLog;

namespace GetCalendarService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class GetCalendar : IGetCalendar
    {
        private readonly string _exchangeServiceUrl = ConfigurationManager.AppSettings["ExchangeServiceUrl"];
        private readonly string _exchangeAuthLogin = ConfigurationManager.AppSettings["ExchangeAuthLogin"];
        private readonly string _exchangeAuthPass = ConfigurationManager.AppSettings["ExchangeAuthPass"];
        private readonly TimeSpan _contactListMutexTimeout = 
            TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["ContactListMutexTimeoutSec"]));

        private readonly int _serverBusyMax = 10;
        private readonly TimeSpan _serverBusyTimeout = TimeSpan.FromMinutes(1);

        private readonly ExchangeManagementService _exchangeSvc;

        private readonly Mutex _contactListMutex = new Mutex(false, "47E21B40-9D63-4C77-A275-BB2607630059");

        public GetCalendar()
        {
            var exchangeSemaphoreTimeout = TimeSpan.FromSeconds(int.Parse(ConfigurationManager.AppSettings["ExchangeSemaphoreTimeoutSec"]));
            var exchnageSemaphoreCount = int.Parse(ConfigurationManager.AppSettings["ExchangeSemaphoreCount"]);
            var exchangeVersion = (ExchangeVersion)Enum.Parse(typeof(ExchangeVersion),
                ConfigurationManager.AppSettings["ExchangeVersion"]);
            _exchangeSvc = new ExchangeManagementService(_exchangeAuthLogin, _exchangeAuthPass,
                _exchangeServiceUrl, exchangeSemaphoreTimeout, exchnageSemaphoreCount, exchangeVersion);
        }

        public List<CalendarEvent> GET_CALENDAR(string email, DateTime start, DateTime end)
        {
            var callerIp = GetCallerIpAddress();
            LogManager.GetCurrentClassLogger().Info($"Executing request from {callerIp}: email={email}, start={start}, end={end}");

            ThreadPool.QueueUserWorkItem(s => AddContactIfDoesNotExist(email));

            try
            {
                Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CalendarContext>());
                var context = new CalendarContext();
                var records = context.CalendarRecords
                    .Where(r => r.CalendarId == email && start <= r.Start && r.Start <= end && !r.IsDeleted).ToArray();
                return records.Select(r => r.ToCalendarEvent()).ToList();
            }
            catch (Exception exc)
            {
                return ConstructExceptionEvent(exc);
            }
        }

        private void AddContactIfDoesNotExist(string email)
        {
            var mutexOwned = false;
            var serverBusyCounter = 0;
            var isEmailInContactList = false;

            // first find if email is on the contact list
            while (serverBusyCounter < _serverBusyMax)
            {
                try
                {
                    mutexOwned = _contactListMutex.WaitOne(_contactListMutexTimeout);
                    if (!mutexOwned) return;
                    isEmailInContactList = IsEmailInContactList(email);
                    _contactListMutex.ReleaseMutex();
                    mutexOwned = false;
                    break;
                }
                catch (ServiceResponseException exc) when (exc.ErrorCode == ServiceError.ErrorServerBusy)
                {
                    if (mutexOwned)
                    {
                        _contactListMutex.ReleaseMutex();
                        mutexOwned = false;
                    }
                    serverBusyCounter++;
                    Thread.Sleep(_serverBusyTimeout);
                }
                catch
                {
                    if(mutexOwned)
                        _contactListMutex.ReleaseMutex();
                    return;
                }
            }

            serverBusyCounter = 0;

            if (isEmailInContactList) return;

            while (serverBusyCounter < _serverBusyMax)
            {
                try
                {
                    mutexOwned = _contactListMutex.WaitOne(_contactListMutexTimeout);
                    if (!mutexOwned) return;
                    _exchangeSvc.AddMailboxToContactList(email, CancellationToken.None);
                    isEmailInContactList = true;
                    _contactListMutex.ReleaseMutex();
                    mutexOwned = false;
                    break;
                }
                catch (ServiceResponseException exc) when (exc.ErrorCode == ServiceError.ErrorServerBusy)
                {
                    if (mutexOwned)
                    {
                        _contactListMutex.ReleaseMutex();
                        mutexOwned = false;
                    }
                    serverBusyCounter++;
                    Thread.Sleep(_serverBusyTimeout);
                }
                catch
                {
                    if (mutexOwned)
                        _contactListMutex.ReleaseMutex();
                    return;
                }
            }

            if (!isEmailInContactList) return;

            var contactListUpdateSignal = new EventWaitHandle(false, EventResetMode.AutoReset,
                            "66E4B327-CD6D-46D3-B158-5066D4813203");
            contactListUpdateSignal.Set();

            //var signalEventHandleName = $"ContactDataUpdated_{email}";
            //var signalEventWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset,
            //    signalEventHandleName);
            //signalEventWaitHandle.Reset();
            ////
            //var sw = new Stopwatch();
            //sw.Start();
            //var signalReceived =
            //    signalEventWaitHandle.WaitOne(TimeSpan.FromSeconds(_contactUpdateTimeoutSec));
            //sw.Stop();
            //Debug.WriteLine(
            //    $"Signal received: {signalReceived}, elapsed: {sw.Elapsed.TotalSeconds.ToString("F2")} seconds");
        }

        private List<CalendarEvent> ConstructExceptionEvent(Exception exc)
        {
            return new List<CalendarEvent>
                {
                    new CalendarEvent
                    {
                        EventId = "-1",
                        Start = DateTime.Now,
                        End = DateTime.Now,
                        Subject = exc.Message,
                        Body = exc.ToString()
                    }
                };
        }

        private bool IsEmailInContactList(string email)
        {
            var contactEmails = _exchangeSvc.GetContactEmails(CancellationToken.None).Result;
            return contactEmails.Contains(email.ToLower());
        }

        private string GetCallerIpAddress()
        {
            try
            {
                var context = OperationContext.Current;
                var properties = context.IncomingMessageProperties;
                var endpoint = properties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                return endpoint?.Address;
            }
            catch (Exception)
            {
                return "Undefined";
            }
        }

        private List<CalendarEvent> Dummy()
        {
            return new List<CalendarEvent>
            {
                CalendarEvent.Dummy(DateTime.Today.AddHours(1), TimeSpan.FromHours(1), "Test 1", "Test 1"),
                CalendarEvent.Dummy(DateTime.Today.AddHours(2), TimeSpan.FromHours(1), "Test 2", "Test 2"),
                CalendarEvent.Dummy(DateTime.Today.AddHours(3), TimeSpan.FromHours(1), "Test 3", "Test 3"),
                CalendarEvent.Dummy(DateTime.Today.AddHours(4), TimeSpan.FromHours(1), "Test 4", "Test 4"),
                CalendarEvent.Dummy(DateTime.Today.AddHours(5), TimeSpan.FromHours(1), "Test 5", "Test 5"),
                CalendarEvent.Dummy(DateTime.Today.AddHours(6), TimeSpan.FromHours(1), "Test 6", "Test 6")
            };
        }
    }
}