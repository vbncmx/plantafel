﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ExchangeManagement;
using Microsoft.Exchange.WebServices.Data;

namespace GetCalendarService
{
    [DataContract]
    public class CalendarEvent
    {
        [DataMember(Name = "EventID")]
        public string EventId { get; set; }

        [DataMember(Name = "Organizer")]
        public Organizer Organizer { get; set; }

        [DataMember(Name = "start")]
        public DateTime Start { get; set; }

        [DataMember(Name = "end")]
        public DateTime End { get; set; }

        [DataMember(Name = "busyStatus")]
        public string BusyStatus { get; set; }

        [DataMember(Name = "allDayEvent")]
        public bool AllDayEvent { get; set; }

        [DataMember(Name = "timeZone")]
        public string TimeZone { get; set; }

        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "location")]
        public string Location { get; set; }

        [DataMember(Name = "body")]
        public string Body { get; set; }

        [DataMember(Name = "priority")]
        public string Priority { get; set; }

        [DataMember(Name = "Categories")]
        public List<string> Categories { get; set; }

        [DataMember(Name = "isPrivate")]
        public bool IsPrivate { get; set; }

        [DataMember(Name = "isCanceled")]
        public bool IsCanceled { get; set; }

        [DataMember(Name = "hasAttachments")]
        public bool HasAttachements { get; set; }

        public static CalendarEvent FromEwsAppointment(Appointment appointment)
        {
            var isPrivate = appointment.Sensitivity == Sensitivity.Private;
            if (isPrivate)
            {
                return new CalendarEvent
                {
                    Start = appointment.Start,
                    End = appointment.End,
                    BusyStatus = appointment.LegacyFreeBusyStatus.ToString(),
                    TimeZone = appointment.TimeZone,
                    IsPrivate = true
                };
            }
            return new CalendarEvent
            {
                EventId = appointment.EventId(),
                AllDayEvent = appointment.IsAllDayEvent,
                Body = appointment.Body.Text,
                BusyStatus = appointment.LegacyFreeBusyStatus.ToString(),
                Categories = appointment.Categories.ToList(),
                End = appointment.End,
                Start = appointment.Start,
                HasAttachements = appointment.HasAttachments,
                IsCanceled = appointment.IsCancelled,
                IsPrivate = false,
                Subject = appointment.Subject,
                Organizer = Organizer.FromEmailAddress(appointment.Organizer),
                Priority = appointment.Importance.ToString(),
                Location = appointment.Location,
                TimeZone = appointment.TimeZone
            };
        }

        public static CalendarEvent Dummy(DateTime start, TimeSpan duration, string subject, string body)
        {
            return new CalendarEvent
            {
                EventId = Guid.NewGuid().ToString(),
                AllDayEvent = false,
                Body = body,
                BusyStatus = LegacyFreeBusyStatus.Busy.ToString(),
                Categories = new List<string>(),
                End = start + duration,
                Start = start,
                HasAttachements = false,
                IsCanceled = false,
                IsPrivate = false,
                Subject = subject,
                Organizer = new Organizer
                {
                    Email = "organizer@test.com",
                    Name = "Mr. Organizer"
                },
                Priority = Importance.Normal.ToString(),
                Location = "Pool",
                TimeZone = "UTC+3"
            };
        }
    }
}